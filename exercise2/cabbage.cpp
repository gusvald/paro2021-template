#include "day-of-year.hpp"
#include <string>
#include <vector>
#include <iostream>

/*
A, E, I, O, U, L, N, R, S, T       1
D, G                               2
B, C, M, P                         3
F, H, V, W, Y                      4
K                                  5
J, X                               8
Q, Z                               10

*/





auto dayOfYear(std::string word) -> int {

    std::vector<char> letter (word.begin(),word.end());
    auto value = 0;

    for (auto &c: letter) {
        if(letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' ||
           letter == 'u' || letter == 'l' || letter == 'n' || letter == 'r' ||
           letter == 's' || letter == 't') {
            value += 1;
        } else if(letter == 'd' || letter == 'g') {
            value += 2;
        } else if(letter == 'b' || letter == 'c' || letter == 'm' ||
                  letter == 'p') {
            value += 3;
        } else if(letter == 'f' || letter == 'h' || letter == 'v' ||
                  letter == 'w' || letter == 'y') {
            value += 4;
        } else if(letter == 'k') {
            value += 5;
        else if(letter == 'j' || letter == 'x') {
            value += 8;
        } else if(letter == 'q' || letter == 'z') {
            value += 10;
        }
    }


    return value;
}
